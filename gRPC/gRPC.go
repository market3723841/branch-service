package grpc

import (
	"gitlab.com/market3723841/branch-service/config"
	"gitlab.com/market3723841/branch-service/gRPC/client"
	"gitlab.com/market3723841/branch-service/gRPC/service"
	branch_service "gitlab.com/market3723841/branch-service/genproto"
	"gitlab.com/market3723841/branch-service/pkg/logger"
	"gitlab.com/market3723841/branch-service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	branch_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	branch_service.RegisterBranchProductServiceServer(grpcServer, service.NewBranchProductService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
